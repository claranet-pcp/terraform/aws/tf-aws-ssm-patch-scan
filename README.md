# tf-aws-ssm-patch-scan

This Terraform module provides OS patching on AWS via SSM.

The module can be used to create scheduled scan (checking for required updates) and patch (install updates) operations on EC2 instances.
There is also a submodule with an IAM Policy configuration that your instance profiles will need to have to use SSM Patch Manager, and also an example of how to configure scanning/patching.

## Prerequisites
Before you use this module, you should have:

1. Agreed with the customer which instances require patching, and grouped them based on their patching requirements.
2. Agreed with the customer which [Patch Baseline](https://docs.aws.amazon.com/systems-manager/latest/userguide/sysman-patch-baselines.html) should be used for each group. If possible, use a predefined baseline.
3. Agreed with the customer what schedule to use for each group of instances.

## Implementation
After the prerequisites have been met, you will then need to:
1. Apply tags to all instances in each group (as this is how this module selects what to patch)

    You can choose any tags names that you like, but the following are recommended:

    **todo - convention

2. Choose the IAM policy that allows SSM Patch Manager to execute correctly. There are a couple of options for this:
   - You can use the standard AWS policy `AmazonSSMManagedInstanceCore`
   - If the `ssm_managed` policy from [the existing iam module](https://gitlab.com/claranet-pcp/terraform/aws/tf-aws-iam-instance-profile) is already being used, you can re-use it.
3. Attach that IAM policy to all roles used as instance profiles for the resources that require scanning/patching.
4. To capture SSM log output, you will need to grant permission to write to your S3 bucket. The module in `./iam-setup` can be used to do this. The policy in this module will also allow instances that do not have internet access to function correctly via an S3 VPC endpoint.

These steps should be done using the same configuration as is used for rest of the customer's IAM setup (to ensure that it is not subsequently detected as drift in the IAM setup, and inadvertently removed).

Then:

4. Install the SSM Agent on all instances in scope for patching.
5. Create an S3 bucket to store SSM log files to aid diagnosis of errors - an example configuration is provided in `./examples/scan-and-patch/s3.tf`
6. Ensure that connectivity is enabled to allow access to S3 and SSM. This can be either via https or by creating VPC endpoints (the example shows the endpoints that need to be created and their configuration)
7. Optionally, enable automatic forwarding of patch compliance to SecurityHub

## One implementation per group per operation
You will need to use this module multiple times - one per group of instances per operation.

For example, if you have three groups: group-1 (amazon Linux), group-2 (windows), group-3 (ubuntu), you are likely to have 6 implementations of this module:

|          | non-production                                                    | production                                                       |
|----------|-------------------------------------------------------------------|------------------------------------------------------------------|
|Group 1   | weekly<br> Tuesdays at 01:00<br>auto patch<br>patch baseline X    | weekly<br> Tuesdays at 01:00<br>scan only<br>patch baseline X    |
|Group 2   | weekly<br> Wednesdays at 01:00<br>auto patch<br>patch baseline Y  | weekly<br> Wednesdays at 01:00<br>scan only<br>patch baseline X  |
|Group 3   | weekly<br> Thursdays at 01:00<br>auto patch<br>patch baseline Z   | weekly<br> Thursdays at 01:00<br>scan only<br>patch baseline X   |


## Example usage:

```hcl
module "scanning" {
  source = "."
  name   = "example_scan"

  patch_baseline_id = "arn:aws:ssm:eu-west-1:845259048710:patchbaseline/pb-03881a2222f401e0f" # This is the arn of the standard Amazon Linux 2 Patch Baseline

  match_tags = {
    "ScanMe" = ["true"] # All EC2 instances with a tag of "ScanMe" set to true will be scanned
  }

  schedule = "cron(15 14 * * ? *)" # Every day at 14:15 please

  ssm_s3_log_bucket = "my-ssm-log-bucket" # the bucket name for log output
  ssm_s3_logprefix  = "example_scan" # use a prefix for different patch groups to make it easier to find log output
}
```

There is a fully functioning example of multiple modules for patching and scanning in `./examples/scan-and-patch`. The most interesting parts are:

`main.tf` shows the scan/patch config
`iam.tf` shows how to create an instance profile that supports Patch Manager
`asgs.tf` and `instances.tf` create several instances - some require patching, some don't. Some have internet access, some don't (and require vpc endpoints). Some are configured for scanning, some for patching and some for neither.

## Creating a bespoke Patch Baseline
This module also allows you to create a bespoke Patch Baseline. To do this, don't specify `patch_baseline_id`.

See the table below for details of the parameters to do this.


<!-- BEGINNING OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
#### Requirements

No requirements.

#### Providers

| Name | Version |
|------|---------|
| <a name="provider_aws"></a> [aws](#provider\_aws) | n/a |

#### Modules

No modules.

#### Resources

| Name | Type |
|------|------|
| [aws_cloudwatch_event_rule.reports](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/cloudwatch_event_rule) | resource |
| [aws_cloudwatch_event_target.reports](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/cloudwatch_event_target) | resource |
| [aws_iam_policy.reports_automation](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_policy) | resource |
| [aws_iam_policy.reports_export](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_policy) | resource |
| [aws_iam_role.aws_ssm_maintenance_window](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role) | resource |
| [aws_iam_role.reports_automation](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role) | resource |
| [aws_iam_role.reports_export](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role) | resource |
| [aws_iam_role_policy_attachment.aws_ssm_maintenance_window](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role_policy_attachment) | resource |
| [aws_iam_role_policy_attachment.reports_automation](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role_policy_attachment) | resource |
| [aws_iam_role_policy_attachment.reports_export](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role_policy_attachment) | resource |
| [aws_s3_bucket.reports_export](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/s3_bucket) | resource |
| [aws_s3_bucket_server_side_encryption_configuration.reports_export](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/s3_bucket_server_side_encryption_configuration) | resource |
| [aws_ssm_maintenance_window.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/ssm_maintenance_window) | resource |
| [aws_ssm_maintenance_window_target.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/ssm_maintenance_window_target) | resource |
| [aws_ssm_maintenance_window_task.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/ssm_maintenance_window_task) | resource |
| [aws_ssm_patch_baseline.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/ssm_patch_baseline) | resource |
| [aws_ssm_patch_group.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/ssm_patch_group) | resource |
| [aws_arn.patch_reports_bucket](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/arn) | data source |
| [aws_caller_identity.current](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/caller_identity) | data source |
| [aws_iam_policy_document.aws_ssm_maintenance_window_policy](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/iam_policy_document) | data source |
| [aws_iam_policy_document.reports_automation](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/iam_policy_document) | data source |
| [aws_iam_policy_document.reports_automation_assume](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/iam_policy_document) | data source |
| [aws_iam_policy_document.reports_export](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/iam_policy_document) | data source |
| [aws_iam_policy_document.reports_export_assume](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/iam_policy_document) | data source |
| [aws_region.current](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/region) | data source |

#### Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_approval_rule_approve_after_days"></a> [approval\_rule\_approve\_after\_days](#input\_approval\_rule\_approve\_after\_days) | Auto approve patches matching the patch filters after this many days | `number` | `7` | no |
| <a name="input_approval_rule_compliance_level"></a> [approval\_rule\_compliance\_level](#input\_approval\_rule\_compliance\_level) | Defines the compliance level for patches approved by the approval rule. Must be one of the values documented here: https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/ssm_patch_baseline#compliance_level | `string` | `"CRITICAL"` | no |
| <a name="input_approval_rule_patch_filters"></a> [approval\_rule\_patch\_filters](#input\_approval\_rule\_patch\_filters) | Approval rule filters as documented at: https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/ssm_patch_baseline#patch_filter | <pre>list(object({<br>    key    = string,<br>    values = list(string)<br>  }))</pre> | <pre>[<br>  {<br>    "key": "CLASSIFICATION",<br>    "values": [<br>      "Security"<br>    ]<br>  },<br>  {<br>    "key": "SEVERITY",<br>    "values": [<br>      "Critical"<br>    ]<br>  }<br>]</pre> | no |
| <a name="input_approve_after_days"></a> [approve\_after\_days](#input\_approve\_after\_days) | Patch Baseline - Approve after x days | `number` | `0` | no |
| <a name="input_create_patch_reports_bucket"></a> [create\_patch\_reports\_bucket](#input\_create\_patch\_reports\_bucket) | Toggle creation of patch reports bucket. | `bool` | `true` | no |
| <a name="input_cutoff"></a> [cutoff](#input\_cutoff) | The number of hours before the end of the Maintenance Window that Systems Manager stops scheduling new tasks for execution. | `number` | `1` | no |
| <a name="input_duration"></a> [duration](#input\_duration) | The duration of the Maintenance Window in hours. | `number` | `3` | no |
| <a name="input_enable_non_security"></a> [enable\_non\_security](#input\_enable\_non\_security) | Boolean enabling the application of non-security updates. | `bool` | `false` | no |
| <a name="input_enable_patch_reports"></a> [enable\_patch\_reports](#input\_enable\_patch\_reports) | Toggle for automatic patch report generation. | `bool` | `false` | no |
| <a name="input_match_tags"></a> [match\_tags](#input\_match\_tags) | Tag names and values against which EC2 instances will be matched in a patch group. This has to be a map of tags with lists of valid tag values. | `map(list(string))` | n/a | yes |
| <a name="input_max_concurrency"></a> [max\_concurrency](#input\_max\_concurrency) | The max number of instances to Scan or Patch concurrently. | `number` | `1` | no |
| <a name="input_max_errors"></a> [max\_errors](#input\_max\_errors) | The maximum errors to allow during patching before aborting. | `number` | `1` | no |
| <a name="input_name"></a> [name](#input\_name) | Resource prefix | `string` | `"tf-aws-ssm-patch-scan"` | no |
| <a name="input_operating_system"></a> [operating\_system](#input\_operating\_system) | Name of the OS that runs on matching instances (see https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/ssm_patch_baseline#operating_system for supported values). | `string` | `"AMAZON_LINUX_2"` | no |
| <a name="input_operation"></a> [operation](#input\_operation) | The operation to perform. This must be either `Scan` or `Install` or both | `list(string)` | <pre>[<br>  "Scan"<br>]</pre> | no |
| <a name="input_patch_baseline_id"></a> [patch\_baseline\_id](#input\_patch\_baseline\_id) | The ARN of the patch baseline to use for patching the instance. This is optional and the module will create one if not specified using options passed in to the module. | `string` | `null` | no |
| <a name="input_patch_group_exists"></a> [patch\_group\_exists](#input\_patch\_group\_exists) | n/a | `bool` | `false` | no |
| <a name="input_patch_reports_bucket_arn"></a> [patch\_reports\_bucket\_arn](#input\_patch\_reports\_bucket\_arn) | ARN of the bucket to store patch reports. Must be provided if `create_patch_reports_bucket` is set to `false`. | `string` | `""` | no |
| <a name="input_patch_reports_schedule_expression"></a> [patch\_reports\_schedule\_expression](#input\_patch\_reports\_schedule\_expression) | n/a | `string` | `"cron(0 0 28 * ? *)"` | no |
| <a name="input_reject_patches"></a> [reject\_patches](#input\_reject\_patches) | List of patches to exclude from Scanning and Patching | `list(any)` | `[]` | no |
| <a name="input_rejected_patches_action"></a> [rejected\_patches\_action](#input\_rejected\_patches\_action) | The action for Patch Manager to take on patches included in the rejected\_patches list. Allow values are ALLOW\_AS\_DEPENDENCY and BLOCK | `string` | `"BLOCK"` | no |
| <a name="input_report_name"></a> [report\_name](#input\_report\_name) | Name of the automated CSV patch report | `string` | `"patch-report"` | no |
| <a name="input_schedule"></a> [schedule](#input\_schedule) | The schedule of the Maintenance Window in the form of a cron or rate expression (see https://docs.aws.amazon.com/AmazonCloudWatch/latest/events/ScheduledEvents.html). | `string` | n/a | yes |
| <a name="input_schedule_timezone"></a> [schedule\_timezone](#input\_schedule\_timezone) | Optional. The timezone in IANA Time Zone Database Format to align the schedule to | `string` | `""` | no |
| <a name="input_ssm_s3_log_bucket"></a> [ssm\_s3\_log\_bucket](#input\_ssm\_s3\_log\_bucket) | The name of the S3 bucket to push SSM Patch Manager logs to | `string` | n/a | yes |
| <a name="input_ssm_s3_logprefix"></a> [ssm\_s3\_logprefix](#input\_ssm\_s3\_logprefix) | S3 bucket object key prefix for the SSM RunCommand output. | `string` | `null` | no |
| <a name="input_tags"></a> [tags](#input\_tags) | A map of tags to assign to the resource | `map(any)` | `{}` | no |

#### Outputs

| Name | Description |
|------|-------------|
| <a name="output_patch_baseline_id"></a> [patch\_baseline\_id](#output\_patch\_baseline\_id) | The patch baseline ID in use by the patch scanning. |
<!-- END OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
