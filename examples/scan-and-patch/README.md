# Example deployment

This is a fully functioning example of patching and scanning. The most interesting parts are:

`main.tf` shows the scan/patch config
`iam.tf` shows how to create an instance profile that supports Patch Manager
`asgs.tf` and `instances.tf` create several instances - some require patching, some don't. Some have internet access, some don't (and require vpc endpoints). Some are configured for scanning, some for patching and some for neither.

To see it in action, set the scanning schedules in `main.tf` to suit your needs and inflate the infrastructure in the playground account.