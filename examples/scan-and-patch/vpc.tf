locals {
  private_subnets = [
    cidrsubnet(var.vpc_cidr, 8, 0),
    cidrsubnet(var.vpc_cidr, 8, 1),
    cidrsubnet(var.vpc_cidr, 8, 2)
  ]
  public_subnets = [
    cidrsubnet(var.vpc_cidr, 8, 3),
    cidrsubnet(var.vpc_cidr, 8, 4),
    cidrsubnet(var.vpc_cidr, 8, 5)
  ]
  intra_subnets = [
    cidrsubnet(var.vpc_cidr, 8, 6),
    cidrsubnet(var.vpc_cidr, 8, 7),
    cidrsubnet(var.vpc_cidr, 8, 8)
  ]
}

module "vpc" {
  source  = "terraform-aws-modules/vpc/aws"
  version = "v2.70.0"

  azs                    = data.aws_availability_zones.available.names
  cidr                   = var.vpc_cidr
  enable_dns_hostnames   = true
  enable_dns_support     = true
  enable_nat_gateway     = true
  name                   = var.vpc_name
  private_subnets        = local.private_subnets
  public_subnets         = local.public_subnets
  intra_subnets          = local.intra_subnets
  one_nat_gateway_per_az = var.single_nat_gateway
  enable_s3_endpoint     = var.enable_s3_vpc_endpoint

  enable_ssm_endpoint              = true
  ssm_endpoint_security_group_ids  = [module.security_group_for_ssm_endpoint.this_security_group_id]
  ssm_endpoint_private_dns_enabled = true

  enable_ec2messages_endpoint              = true
  ec2messages_endpoint_security_group_ids  = [module.security_group_for_ssm_endpoint.this_security_group_id]
  ec2messages_endpoint_private_dns_enabled = true

  enable_ssmmessages_endpoint              = true
  ssmmessages_endpoint_security_group_ids  = [module.security_group_for_ssm_endpoint.this_security_group_id]
  ssmmessages_endpoint_private_dns_enabled = true

  tags = {
    Name        = var.vpc_name
    Environment = var.envname
  }
}

module "security_group_for_ssm_endpoint" {
  source  = "terraform-aws-modules/security-group/aws"
  version = "v3.17.0"

  name        = "ssm-test-sg"
  description = "Security group to allow SSM vpc endpoint to be created"
  vpc_id      = module.vpc.vpc_id

  ingress_cidr_blocks = ["0.0.0.0/0"]
  ingress_rules       = ["https-443-tcp"]
}
