variable "enable_ssm_log_file_expiration" {
  description = "Whether SSM Patch Manager logs should be automatically expired and removed from the S3 bucket"
  type        = string
  default     = "Enabled"
}

variable "ssm_log_file_retention_period" {
  description = "The number of days that SSM Patch Manager logs will be retained for in the S3 bucket"
  type        = number
  default     = 30
}

#tfsec:ignore:AWS002 tfsec:ignore:AWS017
resource "aws_s3_bucket" "ssm_patch_manager_log_bucket" {
  bucket = var.ssm_s3_log_bucket
}

resource "aws_s3_bucket_server_side_encryption_configuration" "ssm_patch_manager_log_bucket" {
  bucket = var.ssm_s3_log_bucket

  rule {
    apply_server_side_encryption_by_default {
      sse_algorithm = "aws:kms"
    }
  }
}

resource "aws_s3_bucket_lifecycle_configuration" "ssm_patch_manager_log_bucket" {
  bucket = var.ssm_s3_log_bucket

  rule {
    id     = "ssm_patch_manager_log_expiration"
    status = var.enable_ssm_log_file_expiration

    expiration {
      days = var.ssm_log_file_retention_period
    }
  }
}
