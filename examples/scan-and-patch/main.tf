module "patch_scanning" {
  source = "../.."
  name   = "claranet-dev-patch-scan"

  enable_patch_reports = true
  report_name          = "claranet-dev"

  patch_baseline_id = var.patch_baseline_id

  match_tags = {
    "ScanMe" = ["true"]
  }

  schedule = "cron(15 14 * * ? *)"

  ssm_s3_log_bucket = var.ssm_s3_log_bucket
  ssm_s3_logprefix  = "web"
}

module "patch_scanning_and_patching" {
  source = "../.."
  name   = "claranet-dev-patch-install"

  operation         = ["Install"]
  patch_baseline_id = var.patch_baseline_id

  match_tags = {
    "ScanMe"      = ["true"]
    "AutoPatchMe" = ["true"]
  }

  schedule = "cron(45 09 * * ? *)"

  ssm_s3_log_bucket = var.ssm_s3_log_bucket
  ssm_s3_logprefix  = "web"
}
