resource "aws_instance" "ancient" {
  ami           = var.ancient_amazon_linux2_ami
  instance_type = "t3.nano"

  subnet_id            = module.vpc.private_subnets[0]
  iam_instance_profile = aws_iam_instance_profile.web_instance_profile.name
  user_data            = base64encode("yum install -y ssm-agent && systemctl enable --now ssm-agent")

  tags = {
    Name        = "ssm-patch-mgr-test-ancient-pet"
    Type        = "pet"
    ScanMe      = "true"
    AutoPatchMe = "false"
  }
}

resource "aws_instance" "ancient_with_ssm_managed_scanned" {
  ami           = var.ancient_amazon_linux2_ami
  instance_type = "t3.nano"

  subnet_id            = module.vpc.private_subnets[0]
  iam_instance_profile = module.ssm_managed_instance_profile.profile_name
  user_data            = base64encode("yum install -y ssm-agent && systemctl enable --now ssm-agent")

  tags = {
    Name        = "ssm-patch-mgr-test-ancient-pet-ssm-managed-scan"
    Type        = "pet"
    ScanMe      = "true"
    AutoPatchMe = "false"
  }
}

resource "aws_instance" "ancient_with_ssm_managed_patched" {
  ami           = var.ancient_amazon_linux2_ami
  instance_type = "t3.nano"

  subnet_id            = module.vpc.private_subnets[0]
  iam_instance_profile = module.ssm_managed_instance_profile.profile_name
  user_data            = base64encode("yum install -y ssm-agent && systemctl enable --now ssm-agent")

  tags = {
    Name        = "ssm-patch-mgr-test-ancient-pet-ssm-managed-patch"
    Type        = "pet"
    ScanMe      = "true"
    AutoPatchMe = "true"
  }
}

module "ssm_managed_instance_profile" {
  source      = "git::ssh://git@gitlab.com/claranet-pcp/terraform/aws/tf-aws-iam-instance-profile.git?ref=v6.0.0"
  name        = "ssm_manged-iam-profile"
  ssm_managed = "1"
}


resource "aws_instance" "ancient_auto_update" {
  ami           = var.ancient_amazon_linux2_with_ssm_agent_ami
  instance_type = "t3.nano"

  subnet_id            = module.vpc.intra_subnets[0]
  iam_instance_profile = aws_iam_instance_profile.web_instance_profile.name
  //user_data            = base64encode("yum install -y ssm-agent && systemctl enable --now ssm-agent")

  tags = {
    Name        = "ssm-patch-mgr-test-ancient-auto-update-pet"
    Type        = "pet"
    ScanMe      = "true"
    AutoPatchMe = "true"
  }
}

resource "aws_instance" "up_to_date" {
  ami           = data.aws_ssm_parameter.aws_ami.value
  instance_type = "t3.nano"

  subnet_id            = module.vpc.intra_subnets[0]
  iam_instance_profile = aws_iam_instance_profile.web_instance_profile.name
  user_data            = base64encode("yum install -y ssm-agent && systemctl enable --now ssm-agent")

  tags = {
    Name   = "ssm-patch-mgr-test-up-to-date-pet"
    Type   = "pet"
    ScanMe = "true"
  }
}

resource "aws_instance" "leave_me_alone" {
  ami           = data.aws_ssm_parameter.aws_ami.value
  instance_type = "t3.nano"

  subnet_id            = module.vpc.private_subnets[0]
  iam_instance_profile = aws_iam_instance_profile.web_instance_profile.name
  user_data            = base64encode("yum install -y ssm-agent && systemctl enable --now ssm-agent")

  tags = {
    Name        = "ssm-patch-mgr-test-leave-me-alone-pet"
    Type        = "pet"
    ScanMe      = "false"
    AutoPatchMe = "false"
  }
}
