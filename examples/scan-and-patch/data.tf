data "aws_availability_zones" "available" {}
data "aws_region" "current" {}

data "aws_ssm_parameter" "aws_ami" {
  name = "/aws/service/ami-amazon-linux-latest/amzn2-ami-hvm-x86_64-gp2"
}
