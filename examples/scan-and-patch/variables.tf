# Ancient Amazon Linux AMI
variable "ancient_amazon_linux2_ami" {
  description = "Ancient Amazon Linux 2 AMI. Used intentionally so there is something to find / patch."
  type        = string
  default     = "ami-0838da7c1467f590c"
}

variable "ancient_amazon_linux2_with_ssm_agent_ami" {
  description = "Ancient Amazon Linux 2 AMI with SSM Agent pre-installed. Used intentionally so there is something to find / patch."
  type        = string
  default     = "ami-06ef683d504edb3b9"
}

variable "patch_baseline_id" {
  description = "The patch baseline to use for this patch group"
  type        = string
  default     = "arn:aws:ssm:eu-west-1:845259048710:patchbaseline/pb-0ae97fd6d8867ff9d"
}

# Environment
variable "envtype" {
  description = "The type of the environment (nonprod / prod)"
  type        = string
  default     = "test"
}

variable "envname" {
  description = "The name of the environment"
  type        = string
  default     = "test"
}

variable "aws_region" {
  description = "The AWS region to create the resources"
  type        = string
  default     = "eu-west-1"
}

variable "ssm_s3_log_bucket" {
  description = "The S3 bucket name to use for patch manager log output"
  type        = string
  default     = "cn-ssm-patch-manager-log-test"
}

variable "vpc_cidr" {
  type    = string
  default = "172.23.0.0/16"
}

variable "vpc_name" {
  type    = string
  default = "ssm-patching-test"
}

variable "single_nat_gateway" {
  type    = bool
  default = true
}

variable "enable_s3_vpc_endpoint" {
  type    = bool
  default = true
}
