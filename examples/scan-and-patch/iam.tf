resource "aws_iam_instance_profile" "web_instance_profile" {
  name = "web_instance_profile"
  role = aws_iam_role.test_iam_role.name
}

resource "aws_iam_role" "test_iam_role" {
  name               = "test-iam-role"
  assume_role_policy = data.aws_iam_policy_document.test_iam_role.json
}

data "aws_iam_policy_document" "test_iam_role" {
  statement {

    actions = [
      "sts:AssumeRole"
    ]
    principals {
      identifiers = ["ec2.amazonaws.com"]
      type        = "Service"
    }
    effect = "Allow"
  }
}

resource "aws_iam_role_policy_attachment" "ssm_core_attachment" {
  role       = aws_iam_role.test_iam_role.name
  policy_arn = "arn:aws:iam::aws:policy/AmazonSSMManagedInstanceCore"
}

resource "aws_iam_role_policy_attachment" "ssm_enhanced_attachment" {
  role       = aws_iam_role.test_iam_role.name
  policy_arn = module.ssm_policy_enhanced.ssm_patch_manager_policy_arn
}

module "ssm_policy_enhanced" {
  source                       = "../../iam-setup"
  region                       = "eu-west-1"
  iam_policy_prefix            = "ssm-test"
  ssm_patch_manager_log_bucket = var.ssm_s3_log_bucket
}
