resource "aws_launch_template" "web_compliant" {
  name_prefix   = "web-compliant"
  image_id      = data.aws_ssm_parameter.aws_ami.value
  instance_type = "t3.nano"

  iam_instance_profile {
    name = aws_iam_instance_profile.web_instance_profile.name
  }

  user_data = base64encode("yum install -y ssm-agent && systemctl enable --now ssm-agent")
}

resource "aws_autoscaling_group" "web_compliant" {
  name_prefix = "web-compliant"
  max_size    = 1
  min_size    = 1

  vpc_zone_identifier = module.vpc.public_subnets

  launch_template {
    id      = aws_launch_template.web_compliant.id
    version = "$Latest"
  }


  tag {
    key                 = "Name"
    value               = "ssm-patch-mgr-test-up-to-date-cattle"
    propagate_at_launch = true
  }

  tag {
    key                 = "ScanMe"
    value               = "true"
    propagate_at_launch = true
  }
}

resource "aws_launch_template" "web_non_compliant" {
  name_prefix   = "web-non-compliant"
  image_id      = var.ancient_amazon_linux2_ami
  instance_type = "t3.nano"

  iam_instance_profile {
    name = aws_iam_instance_profile.web_instance_profile.name
  }

  user_data = base64encode("yum install -y ssm-agent && systemctl enable --now ssm-agent")
}

resource "aws_autoscaling_group" "web_non_compliant" {
  name_prefix = "web-non-compliant"
  max_size    = 1
  min_size    = 1

  vpc_zone_identifier = module.vpc.private_subnets

  launch_template {
    id      = aws_launch_template.web_non_compliant.id
    version = "$Latest"
  }

  tag {
    key                 = "Name"
    value               = "ssm-patch-mgr-test-ancient-cattle"
    propagate_at_launch = true
  }

  tag {
    key                 = "ScanMe"
    value               = "true"
    propagate_at_launch = true
  }
}

resource "aws_launch_template" "web_non_compliant_update_me" {
  name_prefix   = "web-non-compliant_update_me"
  image_id      = var.ancient_amazon_linux2_ami
  instance_type = "t3.nano"

  iam_instance_profile {
    name = aws_iam_instance_profile.web_instance_profile.name
  }

  user_data = base64encode("yum install -y ssm-agent && systemctl enable --now ssm-agent")
}

resource "aws_autoscaling_group" "web_non_compliant_update_me" {
  name_prefix = "web-non-compliant_update_me"
  max_size    = 1
  min_size    = 1

  vpc_zone_identifier = module.vpc.private_subnets

  launch_template {
    id      = aws_launch_template.web_non_compliant_update_me.id
    version = "$Latest"
  }

  tag {
    key                 = "Name"
    value               = "ssm-patch-mgr-test-auto-update-cattle"
    propagate_at_launch = true
  }

  tag {
    key                 = "ScanMe"
    value               = "true"
    propagate_at_launch = true
  }

  tag {
    key                 = "AutoPatchMe"
    value               = "true"
    propagate_at_launch = true
  }
}
