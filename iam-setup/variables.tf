variable "iam_policy_prefix" { type = string }
variable "region" { type = string }
variable "ssm_patch_manager_log_bucket" { type = string }