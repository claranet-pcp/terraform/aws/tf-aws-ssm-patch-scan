# See: https://docs.aws.amazon.com/systems-manager/latest/userguide/setup-instance-profile.html

resource "aws_iam_policy" "ssm_patch_manager_policy" {
  name = "${var.iam_policy_prefix}-ssm-patch-manager-policy"
  description  = "A policy that allows SSM patch manager to run successfully. This policy should be attached to instance profile roles for resources that need patching via SSM"
  policy = data.aws_iam_policy_document.ssm_patch_manager_policy.json
}

data "aws_iam_policy_document" "ssm_patch_manager_policy" {
  statement {
    actions = [
      "s3:GetObject"
    ]
    resources = [
      "arn:aws:s3:::aws-ssm-${var.region}/*",
      "arn:aws:s3:::aws-windows-downloads-${var.region}/*",
      "arn:aws:s3:::amazon-ssm-${var.region}/*",
      "arn:aws:s3:::amazon-ssm-packages-${var.region}/*",
      "arn:aws:s3:::${var.region}-birdwatcher-prod/*",
      "arn:aws:s3:::aws-ssm-distributor-file-${var.region}/*",
      "arn:aws:s3:::aws-ssm-document-attachments-${var.region}/*",
      "arn:aws:s3:::patch-baseline-snapshot-${var.region}/*"
    ]
    effect = "Allow"
  }
  statement {
    actions = [
      "s3:GetObject",
      "s3:PutObject",
      "s3:PutObjectAcl",
      "s3:GetEncryptionConfiguration"
    ]
    resources = [
      "arn:aws:s3:::${var.ssm_patch_manager_log_bucket}/*",
      "arn:aws:s3:::${var.ssm_patch_manager_log_bucket}"
    ]
    effect = "Allow"
  }
}

output "ssm_patch_manager_policy_arn" {
  value = aws_iam_policy.ssm_patch_manager_policy.arn
}