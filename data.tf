data "aws_caller_identity" "current" {}

data "aws_region" "current" {}

data "aws_arn" "patch_reports_bucket" {
  count = var.enable_patch_reports && var.create_patch_reports_bucket == false ? 1 : 0

  arn = var.patch_reports_bucket_arn
}
