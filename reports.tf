locals {
  patch_reports_bucket_id  = var.enable_patch_reports ? (var.create_patch_reports_bucket ? aws_s3_bucket.reports_export[0].id : data.aws_arn.patch_reports_bucket[0].resource) : null
  patch_reports_bucket_arn = var.enable_patch_reports ? (var.create_patch_reports_bucket ? aws_s3_bucket.reports_export[0].arn : var.patch_reports_bucket_arn) : null
}

resource "aws_cloudwatch_event_rule" "reports" {
  count = var.enable_patch_reports ? 1 : 0

  name                = "${var.name}-reports-export"
  description         = "Schedule recurring patch reporting"
  schedule_expression = var.patch_reports_schedule_expression
}

resource "aws_cloudwatch_event_target" "reports" {
  count = var.enable_patch_reports ? 1 : 0

  target_id = "${var.name}-reports-export"
  rule      = aws_cloudwatch_event_rule.reports[0].name
  arn       = "arn:aws:ssm:${data.aws_region.current.name}:${data.aws_caller_identity.current.account_id}:automation-definition/AWS-ExportPatchReportToS3:$DEFAULT"

  input = jsonencode(
    {
      assumeRole = [
        aws_iam_role.reports_export[0].arn
      ]
      reportName = [
        var.report_name
      ]
      s3BucketName = [
        local.patch_reports_bucket_id
      ]
      targets = [
        "instanceids=*",
      ]
    }
  )

  role_arn = aws_iam_role.reports_automation[0].arn
}

resource "aws_iam_role" "reports_automation" {
  count = var.enable_patch_reports ? 1 : 0

  name               = "${var.name}-reports-automation"
  assume_role_policy = data.aws_iam_policy_document.reports_automation_assume[0].json
}

data "aws_iam_policy_document" "reports_automation_assume" {
  count = var.enable_patch_reports ? 1 : 0

  statement {
    actions = [
      "sts:AssumeRole",
    ]

    principals {
      type        = "Service"
      identifiers = ["events.amazonaws.com"]
    }
  }
}

resource "aws_iam_policy" "reports_automation" {
  count = var.enable_patch_reports ? 1 : 0

  name   = "${var.name}-reports-automation"
  policy = data.aws_iam_policy_document.reports_automation[0].json
}

data "aws_iam_policy_document" "reports_automation" {
  count = var.enable_patch_reports ? 1 : 0

  statement {
    actions = [
      "ssm:StartAutomationExecution",
    ]

    resources = [
      "arn:aws:ssm:${data.aws_region.current.name}::automation-definition/AWS-ExportPatchReportToS3:$DEFAULT"
    ]
  }

  statement {
    actions = [
      "iam:PassRole",
    ]

    resources = [
      aws_iam_role.reports_export[0].arn
    ]

    condition {
      test     = "StringLikeIfExists"
      variable = "iam:PassedToService"

      values = [
        "ssm.amazonaws.com"
      ]
    }
  }
}

resource "aws_iam_role_policy_attachment" "reports_automation" {
  count = var.enable_patch_reports ? 1 : 0

  role       = aws_iam_role.reports_automation[0].name
  policy_arn = aws_iam_policy.reports_automation[0].arn
}

resource "aws_iam_role" "reports_export" {
  count = var.enable_patch_reports ? 1 : 0

  name               = "${var.name}-reports-export"
  assume_role_policy = data.aws_iam_policy_document.reports_export_assume[0].json
}

data "aws_iam_policy_document" "reports_export_assume" {
  count = var.enable_patch_reports ? 1 : 0

  statement {
    actions = [
      "sts:AssumeRole",
    ]

    principals {
      type        = "Service"
      identifiers = ["ssm.amazonaws.com"]
    }
  }
}

resource "aws_iam_policy" "reports_export" {
  count = var.enable_patch_reports ? 1 : 0

  name   = "${var.name}-reports-export"
  policy = data.aws_iam_policy_document.reports_export[0].json
}

data "aws_iam_policy_document" "reports_export" {
  count = var.enable_patch_reports ? 1 : 0

  statement {
    actions = [
      "s3:PutObject"
    ]

    resources = [
      "${local.patch_reports_bucket_arn}/*.csv"
    ]
  }

  statement {
    actions = [
      "s3:GetBucketAcl"
    ]

    resources = [
      local.patch_reports_bucket_arn
    ]
  }

  statement {
    actions = [
      "ssm:DescribeInstancePatchStates",
      "ssm:DescribeInstancePatches",
      "ssm:ListComplianceItems",
      "ssm:ListResourceComplianceSummaries",
      "ssm:DescribeInstanceInformation",
      "ssm:GetInventory",
      "ec2:DescribeInstances"
    ]

    resources = [
      "*"
    ]
  }
}

resource "aws_iam_role_policy_attachment" "reports_export" {
  count = var.enable_patch_reports ? 1 : 0

  role       = aws_iam_role.reports_export[0].name
  policy_arn = aws_iam_policy.reports_export[0].arn
}

#tfsec:ignore:AWS002 tfsec:ignore:AWS017
resource "aws_s3_bucket" "reports_export" {
  count = var.enable_patch_reports && var.create_patch_reports_bucket ? 1 : 0

  bucket = "${var.name}-reports-export"
}

resource "aws_s3_bucket_server_side_encryption_configuration" "reports_export" {
  count = var.enable_patch_reports && var.create_patch_reports_bucket ? 1 : 0

  bucket = aws_s3_bucket.reports_export[0].id

  rule {
    apply_server_side_encryption_by_default {
      sse_algorithm = "AES256"
    }
  }
}
