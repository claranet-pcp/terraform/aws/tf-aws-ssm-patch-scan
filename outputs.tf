output "patch_baseline_id" {
  description = "The patch baseline ID in use by the patch scanning."
  value       = var.patch_baseline_id == null ? aws_ssm_patch_baseline.this[0].id : var.patch_baseline_id
}
