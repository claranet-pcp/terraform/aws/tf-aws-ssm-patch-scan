resource "aws_ssm_patch_baseline" "this" {
  count                                = var.patch_baseline_id == null || var.patch_baseline_id == "" ? 1 : 0
  name                                 = "${var.name}-${lower(element(var.operation, 0))}"
  description                          = "${var.name} - ${var.operating_system} patch ${element(var.operation, 0)} baseline"
  operating_system                     = var.operating_system
  rejected_patches                     = var.reject_patches
  rejected_patches_action              = var.rejected_patches_action
  approved_patches_enable_non_security = var.enable_non_security

  approval_rule {
    approve_after_days  = var.approval_rule_approve_after_days
    compliance_level    = var.approval_rule_compliance_level
    enable_non_security = var.enable_non_security

    dynamic "patch_filter" {
      for_each = var.approval_rule_patch_filters
      content {
        key    = patch_filter.value.key
        values = patch_filter.value.values
      }
    }
  }

  tags = var.tags
}

resource "aws_ssm_patch_group" "this" {
  count       = var.patch_group_exists ? 0 : 1
  depends_on  = [aws_ssm_patch_baseline.this]
  baseline_id = var.patch_baseline_id == null || var.patch_baseline_id == "" ? aws_ssm_patch_baseline.this.*.id[0] : var.patch_baseline_id
  patch_group = var.name
}

resource "aws_ssm_maintenance_window" "this" {
  name              = "${var.name}-${lower(element(var.operation, 0))}"
  schedule          = var.schedule
  schedule_timezone = var.schedule_timezone
  duration          = var.duration
  cutoff            = var.cutoff
  tags              = var.tags
}

resource "aws_ssm_maintenance_window_target" "this" {
  window_id     = aws_ssm_maintenance_window.this.id
  name          = "${var.name}-${lower(element(var.operation, 0))}"
  description   = "${var.name} ${lower(element(var.operation, 0))} target"
  resource_type = "INSTANCE"

  dynamic "targets" {
    for_each = var.match_tags

    content {
      key    = "tag:${targets.key}"
      values = targets.value
    }
  }
}

resource "aws_ssm_maintenance_window_task" "this" {
  for_each = toset(var.operation)

  window_id        = aws_ssm_maintenance_window.this.id
  task_type        = "RUN_COMMAND"
  task_arn         = "AWS-RunPatchBaseline"
  priority         = each.value == "Install" ? 1 : 2
  service_role_arn = aws_iam_role.aws_ssm_maintenance_window.arn
  max_concurrency  = var.max_concurrency
  max_errors       = var.max_errors

  targets {
    key    = "WindowTargetIds"
    values = [aws_ssm_maintenance_window_target.this.id]
  }

  task_invocation_parameters {
    run_command_parameters {
      parameter {
        name   = "Operation"
        values = [each.value]
      }
      output_s3_bucket     = var.ssm_s3_log_bucket
      output_s3_key_prefix = var.ssm_s3_logprefix
    }
  }
}

resource "aws_iam_role" "aws_ssm_maintenance_window" {
  name               = "${var.name}-${lower(element(var.operation, 0))}"
  assume_role_policy = data.aws_iam_policy_document.aws_ssm_maintenance_window_policy.json
}

data "aws_iam_policy_document" "aws_ssm_maintenance_window_policy" {
  statement {
    actions = [
      "sts:AssumeRole"
    ]
    principals {
      identifiers = ["ssm.amazonaws.com"]
      type        = "Service"
    }
    effect = "Allow"
  }
}

resource "aws_iam_role_policy_attachment" "aws_ssm_maintenance_window" {
  role       = aws_iam_role.aws_ssm_maintenance_window.name
  policy_arn = "arn:aws:iam::aws:policy/service-role/AmazonSSMMaintenanceWindowRole"
}
