# TODO: add validation
variable "patch_baseline_id" {
  type        = string
  description = "The ARN of the patch baseline to use for patching the instance. This is optional and the module will create one if not specified using options passed in to the module. "
  default     = null
}

variable "name" {
  type        = string
  description = "Resource prefix"
  default     = "tf-aws-ssm-patch-scan"
}

variable "report_name" {
  type        = string
  description = "Name of the automated CSV patch report"
  default     = "patch-report"
}

variable "match_tags" {
  type        = map(list(string))
  description = "Tag names and values against which EC2 instances will be matched in a patch group. This has to be a map of tags with lists of valid tag values."
}

variable "operating_system" {
  type        = string
  description = "Name of the OS that runs on matching instances (see https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/ssm_patch_baseline#operating_system for supported values)."
  default     = "AMAZON_LINUX_2"
}

variable "approve_after_days" {
  type        = number
  description = "Patch Baseline - Approve after x days"
  default     = 0
}

variable "schedule" {
  type        = string
  description = "The schedule of the Maintenance Window in the form of a cron or rate expression (see https://docs.aws.amazon.com/AmazonCloudWatch/latest/events/ScheduledEvents.html)."
}

variable "schedule_timezone" {
  type        = string
  description = "Optional. The timezone in IANA Time Zone Database Format to align the schedule to"
  default     = ""
}

variable "duration" {
  type        = number
  description = "The duration of the Maintenance Window in hours."
  default     = 3
}

variable "cutoff" {
  type        = number
  description = "The number of hours before the end of the Maintenance Window that Systems Manager stops scheduling new tasks for execution."
  default     = 1
}

variable "ssm_s3_logprefix" {
  type        = string
  description = "S3 bucket object key prefix for the SSM RunCommand output."
  default     = null
}

variable "reject_patches" {
  type        = list(any)
  description = "List of patches to exclude from Scanning and Patching"
  default     = []
}

variable "approval_rule_approve_after_days" {
  description = "Auto approve patches matching the patch filters after this many days"
  type        = number
  default     = 7
}

variable "approval_rule_compliance_level" {
  description = "Defines the compliance level for patches approved by the approval rule. Must be one of the values documented here: https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/ssm_patch_baseline#compliance_level"
  default     = "CRITICAL"
}

variable "max_concurrency" {
  description = "The max number of instances to Scan or Patch concurrently."
  type        = number
  default     = 1
}

variable "max_errors" {
  description = "The maximum errors to allow during patching before aborting."
  type        = number
  default     = 1
}

variable "approval_rule_patch_filters" {
  description = "Approval rule filters as documented at: https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/ssm_patch_baseline#patch_filter"
  type = list(object({
    key    = string,
    values = list(string)
  }))

  default = [
    {
      key    = "CLASSIFICATION"
      values = ["Security"]
    },
    {
      key    = "SEVERITY"
      values = ["Critical"]
    }
  ]
}

variable "operation" {
  description = "The operation to perform. This must be either `Scan` or `Install` or both"
  type        = list(string)
  default     = ["Scan"]
}

variable "ssm_s3_log_bucket" {
  description = "The name of the S3 bucket to push SSM Patch Manager logs to"
  type        = string
}

variable "tags" {
  description = "A map of tags to assign to the resource"
  type        = map(any)
  default     = {}
}

variable "patch_group_exists" {
  type    = bool
  default = false
}

variable "rejected_patches_action" {
  type        = string
  default     = "BLOCK"
  description = "The action for Patch Manager to take on patches included in the rejected_patches list. Allow values are ALLOW_AS_DEPENDENCY and BLOCK"
}

variable "enable_non_security" {
  type        = bool
  default     = false
  description = "Boolean enabling the application of non-security updates."
}

variable "enable_patch_reports" {
  type        = bool
  default     = false
  description = "Toggle for automatic patch report generation."
}

variable "patch_reports_bucket_arn" {
  type        = string
  default     = ""
  description = "ARN of the bucket to store patch reports. Must be provided if `create_patch_reports_bucket` is set to `false`."
}

variable "create_patch_reports_bucket" {
  type        = bool
  default     = true
  description = "Toggle creation of patch reports bucket."
}

variable "patch_reports_schedule_expression" {
  type    = string
  default = "cron(0 0 28 * ? *)"
}
